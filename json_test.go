package pranala_test

import (
	"encoding/json"
	"os"
	"testing"
)

type jsonTestData struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	Age   int64  `json:"age"`
}

func TestJSON(t *testing.T) {

	//Read File
	file, err := os.ReadFile("test.json")
	if err != nil {
		t.Fatalf("Failed on opening file, err -> %v", err)
	}

	var jsonData jsonTestData
	err = json.Unmarshal(file, &jsonData)
	if err != nil {
		t.Fatalf("Failed on unmarshal, err -> %v", err)
	}

	//====> Unwrap This For first time
	//if jsonData.Name != "John Doe" {
	//	t.Fatalf("Failed ! Expected name to be %v, but got %v", "John Doe", jsonData.Name)
	//}
	//if jsonData.Email != "john.doe@example.com" {
	//	t.Fatalf("Failed ! Expected name to be %v, but got %v", "john.doe@example.com", jsonData.Email)
	//}
	//if jsonData.Age != 30 {
	//	t.Fatalf("Failed ! Expected name to be %v, but got %v", "John Doe", jsonData.Age)
	//}

	//Modified Value
	jsonData.Email = "johndoe@example.com"
	jsonData.Age += 1

	//Turn Back File into JSON
	jsonByte, err := json.Marshal(jsonData)
	if err != nil {
		t.Fatalf("Failed on converting JSON, err -> %v ", err)
	}

	//Write Back JSON
	err = os.WriteFile("test.json", jsonByte, 0644)
	if err != nil {
		t.Fatalf("Failed on write file, err -> %v", err)
	}

}
