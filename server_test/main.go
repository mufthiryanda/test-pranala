package main

import (
	"github.com/labstack/echo/v4"
	items3 "pranala-test/server_test/app/handler/items"
	"pranala-test/server_test/app/repository/item"
	items2 "pranala-test/server_test/app/service/items"
	"pranala-test/server_test/config"
	"pranala-test/server_test/model/items"
)

func main() {

	databaseConfig := config.DatabaseConfig{
		Username:     "root",
		Password:     "Root@123",
		DatabaseName: "pranala_test",
		Host:         "localhost",
		Port:         "3306",
	}

	dbConnection := config.NewConnection(databaseConfig)
	err := items.MigrateAndSeedItem(dbConnection)
	if err != nil {
		panic("Failed to Seed Items")
	}

	itemRepository := item.NewRepository(dbConnection, dbConnection)
	itemService := items2.NewService(dbConnection, dbConnection, itemRepository)
	itemHandler := items3.Handler{ItemService: itemService}

	e := echo.New()
	e.POST("/items/create", itemHandler.CreateItem)
	e.PUT("/items/update", itemHandler.UpdateItem)
	e.DELETE("/items/delete", itemHandler.DeleteItem)
	e.GET("/items/get", itemHandler.GetAllItem)
	e.Logger.Fatal(e.Start(":8000"))

}
