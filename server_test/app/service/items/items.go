package items

import (
	"gorm.io/gorm"
	"pranala-test/server_test/app/repository/item"
	"pranala-test/server_test/model/items"
)

type Param struct {
	WriteDb  *gorm.DB
	ReadDb   *gorm.DB
	itemRepo item.Repository
}

func NewService(writeDb, readDb *gorm.DB, itemRep item.Repository) *Param {
	return &Param{
		WriteDb:  writeDb,
		ReadDb:   readDb,
		itemRepo: itemRep,
	}
}

type Service interface {
	GetAllItem(keyword string) (interface{}, map[string]string, error)
	CreateItem(item items.Item) (interface{}, map[string]string, error)
	UpdateItem(items items.Item) (interface{}, map[string]string, error)
	DeleteItem(itemId int64) (interface{}, map[string]string, error)
}

func (p Param) GetAllItem(keyword string) (interface{}, map[string]string, error) {

	resp, err := p.itemRepo.GetAllItem(keyword)
	if err != nil {
		return nil, map[string]string{
			"en": "Failed to get All Item",
			"id": "Gagal dalam mendapatkan item",
		}, err
	}

	return resp, map[string]string{
		"en": "Successfully",
		"id": "Sukses",
	}, nil
}

func (p Param) CreateItem(item items.Item) (interface{}, map[string]string, error) {

	tx := p.WriteDb.Begin()
	defer tx.Rollback()

	err := p.itemRepo.CreateItem(item, tx)
	if err != nil {
		return nil, map[string]string{
			"en": "Failed to create item, please try again",
			"id": "Gagal membuat item, silahkan coba lagi",
		}, nil
	}

	err = tx.Commit().Error
	if err != nil {
		return nil, map[string]string{
			"en": "Failed to create item, please try again",
			"id": "Gagal membuat item, silahkan coba lagi",
		}, nil
	}

	return nil, map[string]string{
		"en": "Successfully",
		"id": "Sukses",
	}, nil

}

func (p Param) UpdateItem(item items.Item) (interface{}, map[string]string, error) {

	tx := p.WriteDb.Begin()
	defer tx.Rollback()

	err := p.itemRepo.UpdateItem(item, tx)
	if err != nil {
		return nil, map[string]string{
			"en": "Failed to create item, please try again",
			"id": "Gagal update item, silahkan coba lagi",
		}, nil
	}

	err = tx.Commit().Error
	if err != nil {
		return nil, map[string]string{
			"en": "Failed to update item, please try again",
			"id": "Gagal update item, silahkan coba lagi",
		}, nil
	}

	return nil, map[string]string{
		"en": "Successfully",
		"id": "Sukses",
	}, nil

}

func (p Param) DeleteItem(itemId int64) (interface{}, map[string]string, error) {

	tx := p.WriteDb.Begin()
	defer tx.Rollback()

	err := p.itemRepo.DeleteItem(itemId, tx)
	if err != nil {
		return nil, map[string]string{
			"en": "Failed to delete item, please try again",
			"id": "Gagal hapus item, silahkan coba lagi",
		}, nil
	}

	err = tx.Commit().Error
	if err != nil {
		return nil, map[string]string{
			"en": "Failed to delete item, please try again",
			"id": "Gagal hapus item, silahkan coba lagi",
		}, nil
	}

	return nil, map[string]string{
		"en": "Successfully",
		"id": "Sukses",
	}, nil

}
