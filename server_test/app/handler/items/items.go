package items

import (
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	items2 "pranala-test/server_test/app/service/items"
	"pranala-test/server_test/model/items"
	"pranala-test/server_test/utils"
	"strconv"
)

type Handler struct {
	ItemService items2.Service
}

func NewHandler(service items2.Service) *Handler {
	return &Handler{
		ItemService: service,
	}
}

func (h *Handler) CreateItem(ctx echo.Context) error {

	var createData items.ItemDTO
	err := ctx.Bind(&createData)
	if err != nil {
		return utils.CreateResponse(400, "Failed ! Invalid request body", nil, ctx)
	}

	data, msg, err := h.ItemService.CreateItem(items.Item{Name: createData.Name})
	if err != nil {
		log.Println(err)
		return utils.CreateResponse(http.StatusInternalServerError, msg["en"], data, ctx)
	}

	return utils.CreateResponse(http.StatusCreated, msg["id"], data, ctx)

}

func (h *Handler) UpdateItem(ctx echo.Context) error {

	var updateData items.Item
	err := ctx.Bind(&updateData)
	if err != nil {
		return utils.CreateResponse(400, "Failed ! Invalid request body", nil, ctx)
	}

	data, msg, err := h.ItemService.UpdateItem(updateData)
	if err != nil {
		log.Println(err)
		return utils.CreateResponse(http.StatusInternalServerError, msg["en"], data, ctx)
	}

	return utils.CreateResponse(http.StatusCreated, msg["id"], data, ctx)

}

func (h *Handler) DeleteItem(ctx echo.Context) error {

	id := ctx.QueryParam("id_item")
	idItem, err := strconv.Atoi(id)
	if err != nil {
		return utils.CreateResponse(400, "Failed ! Invalid item ud", nil, ctx)
	}

	data, msg, err := h.ItemService.DeleteItem(int64(idItem))
	if err != nil {
		log.Println(err)
		return utils.CreateResponse(http.StatusInternalServerError, msg["en"], data, ctx)
	}

	return utils.CreateResponse(http.StatusCreated, msg["id"], data, ctx)

}

func (h *Handler) GetAllItem(ctx echo.Context) error {

	keyword := ctx.QueryParam("keyword")

	data, msg, err := h.ItemService.GetAllItem(keyword)
	if err != nil {
		log.Println(err)
		return utils.CreateResponse(http.StatusInternalServerError, msg["en"], data, ctx)
	}

	return utils.CreateResponse(http.StatusCreated, msg["id"], data, ctx)

}
