package item

import (
	"gorm.io/gorm"
	"pranala-test/server_test/model/items"
)

type Param struct {
	WriteDb *gorm.DB
	ReadDb  *gorm.DB
}

func NewRepository(writeDb, readDb *gorm.DB) *Param {

	return &Param{
		WriteDb: readDb,
		ReadDb:  writeDb,
	}

}

type Repository interface {
	GetAllItem(keyword string) ([]items.Item, error)
	CreateItem(item items.Item, tx *gorm.DB) error
	UpdateItem(items items.Item, tx *gorm.DB) error
	DeleteItem(itemId int64, tx *gorm.DB) error
}

func (p Param) GetAllItem(keyword string) ([]items.Item, error) {

	var result []items.Item
	statement := p.ReadDb.Table("items")
	if keyword != "" {
		statement.Where("name like ?", `%`+keyword+`%`)
	}
	if err := statement.Scan(&result).Error; err != nil {
		if err != gorm.ErrRecordNotFound {
			return nil, err
		}
	}
	return result, nil

}

func (p Param) CreateItem(item items.Item, tx *gorm.DB) error {
	if err := tx.Create(&item).Error; err != nil {
		return err
	}
	return nil
}

func (p Param) UpdateItem(item items.Item, tx *gorm.DB) error {
	err := tx.Model(&items.Item{}).
		Where("id = ?", item.Id).
		Updates(item).Error
	return err
}

func (p Param) DeleteItem(itemId int64, tx *gorm.DB) error {
	if err := tx.Where("id = ?", itemId).Delete(&items.Item{}).Error; err != nil {
		return err
	}
	return nil
}
