package items

import "gorm.io/gorm"

type Item struct {
	Id   int64  `json:"id" gorm:"column:id;primaryKey;autoIncrement"`
	Name string `json:"name" gorm:"column:name;type:varchar(255)"`
}

func (Item) TableName() string {
	return "items"
}

func MigrateAndSeedItem(db *gorm.DB) error {

	listItem := []Item{
		{
			Name: "Item-1",
		},
		{
			Name: "Item-2",
		},
		{
			Name: "Item-3",
		},
		{
			Name: "Item-4",
		},
		{
			Name: "Item-5",
		},
	}

	err := db.Migrator().DropTable(&Item{})
	if err != nil {
		return err
	}
	err = db.Migrator().AutoMigrate(&Item{})
	if err != nil {
		return err
	}

	err = db.CreateInBatches(listItem, len(listItem)).Error
	return nil

}
