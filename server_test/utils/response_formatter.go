package utils

import (
	"github.com/labstack/echo/v4"
)

func CreateResponse(status int, message string, data interface{}, ctx echo.Context) error {
	return ctx.JSON(status, map[string]interface{}{
		"code":    status,
		"message": message,
		"data":    data,
	})
}
